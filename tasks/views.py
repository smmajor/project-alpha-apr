from django.shortcuts import render, get_object_or_404, redirect
from .models import Task
from django.contrib.auth.decorators import login_required
from .forms import TaskForm

# Create your views here.


@login_required
def show_task(request, id):
    task = get_object_or_404(Task, id=id)
    context = {"task": task}
    return render(request, "tasks/detail.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()

    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def list_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/list.html", context)
